#!/bin/bash

docker build -t registry.gitlab.com/neos-dev-containers/images/dotnet:5.0 --build-arg VARIANT="5.0" . 
docker build -t registry.gitlab.com/neos-dev-containers/images/dotnet:3.1 --build-arg VARIANT="3.1" . 
docker build -t registry.gitlab.com/neos-dev-containers/images/dotnet:2.1 --build-arg VARIANT="2.1" .
