#!/bin/bash

# Define Solution and WebApi project name
read -p "Solution Name: " SOLUTION_NAME
read -p "WebApi Project Name: " PROJECT_NAME
TEST_PROJECT_NAME="${PROJECT_NAME}Test"

# Create Solution in the same folder
dotnet new sln -o "${SOLUTION_NAME}"
mv "${SOLUTION_NAME}/${SOLUTION_NAME}.sln" "./${SOLUTION_NAME}.sln"
rm -rf "${SOLUTION_NAME}"

# Create Project
dotnet new webapi -o "${PROJECT_NAME}"
# Create Test Project
dotnet new mstest -o "${TEST_PROJECT_NAME}"

# Add Project Reference to Test Project
dotnet add "./${TEST_PROJECT_NAME}/${TEST_PROJECT_NAME}.csproj" \
 reference "./${PROJECT_NAME}/${PROJECT_NAME}.csproj"

# Add Project and Test Project to Solution
dotnet sln "${SOLUTION_NAME}.sln" add \
    "./${PROJECT_NAME}/${PROJECT_NAME}.csproj" \
    "./${TEST_PROJECT_NAME}/${TEST_PROJECT_NAME}.csproj"
