#!/bin/bash

docker push registry.gitlab.com/neos-dev-containers/images/dotnet:5.0 
docker push registry.gitlab.com/neos-dev-containers/images/dotnet:3.1
docker push registry.gitlab.com/neos-dev-containers/images/dotnet:2.1
