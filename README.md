# DotNet

You should create the file structure bellow:

```text
 📁 DevEnvironment
 ├─ 📁 .devcontainer
 │  ├─ 📜 devcontainer.json
 │  └─ 📜 docker-compose.yml
 │
 └─ 📁 Solution
    ├─ 📘 Solution.sln
    ├─ 📁 Project
    │  ├─ 📜 appsettings.json
    │  ├─ 📄 Program.cs
    │  ├─ 📄 Startup.cs
    │  └─ 📗 Project.proj
    │
    ├─ 📁 ProjectTest
    │  ├─ 📄 ProgramTest.cs
    │  ├─ 📄 StartupTest.cs
    │  └─ 📙 ProjectTest.proj
    │
    └─ 📁 docs
      ├─ 📃 README.md
      └─ 📃 diagram.puml
```

The ```📁 .devcontainer``` is where your environment is configured. 

The Solution folder is a suggestion for Folder Structure for DotNet core applications.

To generate a dotnet core solution with webapi proj and tests, you can run ```CreateSolution``` inside your container workspace.

The important files will be described bellow:

---

## .devcontainer/devcontainer.json

The extensions section is an example with suggestions for DotNet development. You can add or remove to meet your necessities.

```json
{
    "name": "DotNet & SqlServer",
    "dockerComposeFile": "docker-compose.yml",
    "service": "app",
    "workspaceFolder": "/Solution",
    "extensions":
    [
        "doggy8088.netcore-extension-pack",
        "formulahendry.dotnet",
        "Fudge.auto-using",
        "ryanluker.vscode-coverage-gutters",
        "adrianwilczynski.asp-net-core-snippet-pack",
        "humao.rest-client",
        "yzhang.markdown-all-in-one",
        "jebbs.plantuml",
        "esbenp.prettier-vscode",
        "VisualStudioExptTeam.vscodeintellicode",
        "deerawan.vscode-dash",
        "eamodio.gitlens",
        "oderwat.indent-rainbow",
        "CoenraadS.bracket-pair-colorizer"
    ],
    "settings": {
        "terminal.integrated.shell.linux": "/bin/zsh",
        "editor.fontLigatures": true,
        "editor.minimap.renderCharacters": false,
        "editor.quickSuggestions": true,
        "editor.suggestOnTriggerCharacters": true,
        "omnisharp.enableRoslynAnalyzers": true,
        "omnisharp.enableEditorConfigSupport": true,
        "todohighlight.include": [
            "**/*.js",
            "**/*.jsx",
            "**/*.ts",
            "**/*.tsx",
            "**/*.html",
            "**/*.php",
            "**/*.css",
            "**/*.scss",
            "**/*.cs"
          ]
    }
}
```

## .devcontainer/docker-compose.yml

You can use any other services related with your application or development process. The MSSQL is an example.

The ```app``` section name is connected with the container where you will be working with (in the ```devcontainer.json > service```).

It's importante to notice that volume section is mapping to your Workspace folder. This is important to connect the remote container to your local environment.

If you need another dotnet core version you can choose one of the tags available: ```[5.0, 3.1, 2.1]```

```yaml
services:
  app:
    image: registry.gitlab.com/neos-dev-containers/images/dotnet:5.0
    volumes:
      - ../Solution:/Solution
    command: sleep infinity

  db:
    image: mcr.microsoft.com/mssql/server:2019-latest
    environment:
      SA_PASSWORD: "!mypass123"
      ACCEPT_EULA: "Y"
```

